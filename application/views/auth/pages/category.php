<div class="container">
    <div class="row">
        <div class="col-sm-6 offset-3" id="categoryGrid">

            <div class="headerGrid"></div>

            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#collapseOne">
                        <a class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                           aria-controls="collapseOne">
                            <h6 class="mb-0">
                                Individual Membership
                            </h6>
                        </a>
                    </div>

                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            <h6>
                                <b>
                                    <i class="fa fa-info-circle"></i>
                                    &nbsp;Membership Information
                                </b>
                            </h6>
                            <hr>
                            <ol>
                                <li>
                                    Complete Individual Membership Application Form
                                </li>
                                <li>
                                    Pay a non-refundable registration fee of KSh. 1000
                                </li>
                                <li>
                                    Purchase a minimum of 4 share @ KSh. 250 each
                                </li>
                                <li>
                                    Open an individual saving account(s). <br>
                                    <ul>
                                        <li>
                                            Petabyte
                                        </li>
                                        <li>
                                            Gigabyte
                                        </li>
                                        <li>
                                            Megabyte
                                        </li>
                                        <li>
                                            Kilobyte
                                        </li>
                                    </ul>
                                </li>
                            </ol>
                            <hr>
                            <a class="btn btn-outline-danger" href="<?php echo base_url('auth/signup/individual/'.md5(uniqid()))?>">
                                Sign Up
                            </a>
                        </div>
                    </div>
                </div>
                <br>
                <div class="card">
                    <div class="card-header" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo">
                        <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo"
                           aria-expanded="false" aria-controls="collapseTwo">
                            <h6 class="mb-0">
                                Corporate Membership
                            </h6>
                        </a>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                            <h6>
                                <b>
                                    <i class="fa fa-info-circle"></i>
                                    &nbsp;Membership Information
                                </b>
                            </h6>
                            <hr>
                            <ol>
                                <li>
                                    Complete Corporate Membership Application Form
                                </li>
                                <li>
                                    Pay a non-refundable registration fee of KSh. 1000
                                </li>
                                <li>
                                    Purchase a minimum of 4 share @ KSh. 250 each
                                </li>
                                <li>
                                    Open a corporate saving account. <br>
                                    <ul>
                                        <li>
                                            Petabyte
                                        </li>
                                    </ul>
                            </ol>
                            <hr>
                            <a class="btn btn-outline-danger" href="<?php echo base_url('auth/signup/corporate/'.md5(uniqid()))?>">
                                Sign Up
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>