<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class General extends CI_Model
{
    public function get_counties()
    {
        $query = $this->db->get('counties')->result();

        return $query;
    }

    public function check_email_users($email)
    {
        $query = $this->db->get_where('users', array('email' => $email))->num_rows();

        return $query;
    }

    public function check_phone_users($phone)
    {
        $query = $this->db->get_where('users', array('phone' => $phone))->num_rows();

        return $query;
    }

    public function check_email_users_corp($email)
    {
        $query = $this->db->get_where('corporates', array('office_email' => $email))->num_rows();

        return $query;
    }

    public function check_name_users_corp($name)
    {
        $query = $this->db->get_where('corporates', array('name' => $name))->num_rows();

        return $query;
    }

    public function check_phone_users_corp($phone)
    {
        $query = $this->db->get_where('corporates', array('office_tel' => $phone))->num_rows();

        return $query;
    }

    public function get_user_information($id)
    {
        $details = array();

        $is_user = $this->db->get_where('users', array('user_id' => $id));
        $wallet_amount = $this->db->get_where('user_wallet', array('user_id' => $id))->row('amount');
        $c_shares = $this->check_user_min_shares($id);
        $c_shares_value = $this->check_user_min_shares_value($id);

        $c_contibs = $this->check_user_contribution_months($id);
        $c_contribs_value = $this->check_user_contribution_value($id);

        if($is_user->num_rows() > 0)
        {
            array_push($details,array("name" => $is_user->row('fname')." ".$is_user->row('lname')));
            array_push($details,array("image" => $is_user->row('image')));
            array_push($details, array("wallet" => $wallet_amount));
            array_push($details,array("phone" => $is_user->row('phone')));
            array_push($details,array("acc_no" => $is_user->row('acc_no')));
            array_push($details,array("type_id" => $is_user->row('type_id')));
            array_push($details,array("no_shares" => $c_shares));
            array_push($details,array("total_shares" => $c_shares_value));
            array_push($details,array("contrib_months" => $c_contibs));
            array_push($details,array("total_contribs" => $c_contribs_value));
        }

        else
        {
            $c_data = $this->db->get_where('corporates', array('corporate_id' => $id));

            array_push($details,array("name" => $c_data->row('name')));
            array_push($details,array("image" => $c_data->row('image')));
            array_push($details, array("wallet" => $wallet_amount));
            array_push($details,array("phone" => $c_data->row('c_p_phone')));
            array_push($details,array("acc_no" => $c_data->row('acc_no')));
            array_push($details,array("type_id" => $c_data->row('type_id')));
            array_push($details,array("no_shares" => $c_shares));
            array_push($details,array("total_shares" => $c_shares_value));
            array_push($details,array("contrib_months" => $c_contibs));
            array_push($details,array("total_contribs" => $c_contribs_value));
        }

        return $details;
    }

    public function get_user_id($phone)
    {
        $this->db->where('phone', $phone);
        $query = $this->db->get('users');

        if($query->num_rows() > 0)
        {
            return $query->row('user_id');
        }

        elseif ($query->num_rows() == 0)
        {
            $this->db->where('c_p_phone', $phone);
            $query2 = $this->db->get('corporates');

            if($query2->num_rows() > 0)
            {
                return $query2->row('corporate_id');
            }

            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public function update_wallet($user_id, $amount, $type, $trans_id)
    {
        $data_top_up = array
        (
            'user_id' => $user_id,
            'amount' => $amount,
            'trans_type' => $type,
            'trans_id' => $trans_id
        );

        $this->db->insert('wallet_top_ups', $data_top_up);

        $wallet_amount = $this->db->get_where('user_wallet', array('user_id' => $user_id));

        if($wallet_amount->num_rows() > 0)
        {
            $new_amount = $wallet_amount->row('amount') + $amount;

            $this->db->where('user_id', $user_id);
            $this->db->set('amount', $new_amount);
            $this->db->update('user_wallet');
        }

        else
        {
            $data_wallet = array
            (
                'user_id' => $user_id,
                'amount' => $amount
            );

            $this->db->insert('user_wallet', $data_wallet);
        }

    }
    public function update_wallet_direct($user_id, $amount)
    {
        $this->db->where('user_id', $user_id);
        $this->db->set('amount', $amount);
        $this->db->update('user_wallet');
    }

    public function get_wallet_amount($user_id)
    {
        $this->db->where('user_id', $user_id);
        $query = $this->db->get('user_wallet')->row('amount');

        return $query;
    }

    public function check_user_reg_fee_paid($user_id)
    {
        $this->db->where('user_id', $user_id);
        $query = $this->db->get('user_reg_fee')->num_rows();

        return $query;
    }

    public function check_user_min_shares($user_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->select_sum('no_shares');
        $query = $this->db->get('user_shares')->row('no_shares');

        return $query;
    }

    public function check_user_min_shares_value($user_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->select_sum('amount');
        $query = $this->db->get('user_shares')->row('amount');

        return $query;
    }

    public function check_user_contribution_months($user_id)
    {
        $this->db->where('user_id', $user_id);
        $query = $this->db->get('contributions')->num_rows();

        return $query;
    }

    public function check_user_contribution_value($user_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->select_sum('amount');
        $query = $this->db->get('contributions')->row('amount');

        return $query;
    }

    public function get_user_activities($id)
    {
        $this->db->where('user_id', $id);
        $this->db->order_by('id', "DESC");
        $query = $this->db->get('activities')->result();

        return $query;
    }

    public function check_user_has_account($id)
    {
        $has_account = null;

        $is_user = $this->db->get_where('users', array('user_id' => $id));

        if($is_user->num_rows() > 0)
        {
            $has_account = $is_user->row('type_id');
        }
        else
        {
            $is_corp = $this->db->get_where('corporates', array('corporate_id' => $id));

            if($is_corp->num_rows() > 0)
            {
                $has_account = $is_corp->row('type_id');
            }
        }

        return $has_account;
    }

    public function email($msg, $sub, $to)
    {
        require_once 'phpmailer/PHPMailerAutoload.php';

        $m = new PHPMailer;
        $m->isSMTP();
        $m->SMTPAuth = true;
        $m->SMTPDebug = false;

        $m->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

        $m->Host = 'mail.gated.co.ke';
        $m->Username = 'noreply@gated.co.ke';
        $m->Password = '@noreply@gated2018';
        $m->SMTPSecure = 'tls';
        $m->Port = 25;

        $m->From = 'noreply@gated.co.ke';
        $m->FromName = 'Gated by E-Kodi';
        $m->addReplyTo($to, 'Reply Address');
        $m->addAddress($to, $to);

        $m->Subject = $sub;
        $m->Body = $msg;
        $m->AltBody = $msg;


        if ($m->send()) {
            return true;
        } else {
            return false;
        }

    }
}