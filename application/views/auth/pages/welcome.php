<div class="welcomePageGrid">
    <div class="welcomeGrid">
        <div class="headerGrid"></div>
        <div class="imageGrid"></div>
        <div class="welcomeMessage">
            <h2>Welcome</h2>
        </div>
        <div class="signUpGroup">
            <a type="button" href="<?php echo base_url('auth/category')?>" style="background: #b32017; color: #ffffff" class="formSubmitButton">
                Sign Up
            </a>
        </div>
        <p class="signInText">
            Already have an account? <a href="<?php echo base_url('auth/login')?>"> Sign in</a>
        </p>
        <a
            href="http://www.tisk.co.ke"
            target="blank"
            class="moreInfoText"
        >
            <p>www.tisk.co.ke</p>
        </a>
    </div>
</div>