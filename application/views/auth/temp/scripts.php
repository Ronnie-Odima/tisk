<script src="<?php echo base_url('assets/external/js/jquery.js') ;?>"></script>

<script src="<?php echo base_url('assets/bootstrap/js/bootstrap.js')?>"></script>

<script src="<?php echo base_url('assets/external/sweetalert2/sweetalert2.js') ;?>"></script>

<script src="<?php echo base_url('assets/external/js/custom.js') ;?>"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script type="text/javascript">
    $(function () {
        $("#date_field").datepicker({
            useCurrent: true,
            showClose: true,
            changeYear: true,
            changeMonth: true,
            yearRange: '-60:-18'
        });
    });

</script>

<script type="text/javascript">
    $(function () {

        $('#date_field').datepicker('setDate', new Date(new Date().setFullYear(new Date().getFullYear() - 18)));

    });

</script>

<script type="text/javascript">
    $(function () {
        $("#date_field_corp").datepicker({
            useCurrent: true,
            showClose: true,
            changeYear: true,
            changeMonth: true
        });


    });

</script>

<script type="text/javascript">
    (function check_contrib_dates() {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('payments/pay_monthly_contributions')?>",
            success: function () {

            },
            complete: function () {
                setTimeout(check_contrib_dates, 1000);
            }
        });
    })();
</script>

</body>
</html>