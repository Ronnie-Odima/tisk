<div class="container">
    <div class="row">
        <div class="col-sm-4" style="margin-bottom: 15px;">
            <div class="card">
                <div class="card-body">
                    <h5>
                        <i class="fa fa-google-wallet"></i>
                        &nbsp;
                        My Wallet Amount
                    </h5>
                    <br>

                    <div id="user_wallet_div"></div>

                    <hr>

                    <br>

                    <h6>
                        <i class="fa fa-check-circle"></i>
                        &nbsp;
                        Top Up Your Wallet
                    </h6>

                    <form action="<?php echo base_url('payments/stkPush')?>" method="post" id="top_up_wallet_form">
                        <div class="form-group">
                            <input type="number" class="form-control" name="amount" placeholder="Enter Amount" required>
                            <input type="hidden" name="phone" value="<?php echo $details[3]['phone']?>">
                            <input type="hidden" name="acc_no" value="<?php echo $details[4]['acc_no']?>">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-danger" type="submit">
                                Top Up Wallet
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <h6>
                                <b>
                                    Share Capital
                                </b>
                            </h6>
                            <hr>
                            <h6>
                                <i class="fa fa-bookmark"></i>
                                &nbsp;
                                No. of Shares
                                <b>
                                    &nbsp;
                                    <?php echo $details[6]['no_shares']?>
                                </b>
                            </h6>
                            <h6>
                                <i class="fa fa-money"></i>
                                &nbsp;
                                Total Value
                                <b>
                                    &nbsp;
                                    Ksh. &nbsp;<?php echo number_format($details[7]['total_shares']) ;?>
                                </b>
                            </h6>
                            <br>
                            <button class="btn btn btn-danger btn-sm" data-toggle="modal" data-target="#sharesModal" id="buy_shares_btn">
                                Buy Shares
                            </button>
                            <form action="<?php echo base_url('payments/buy_shares')?>" method="post" id="buy_shares_form">
                                <div id="sharesModal"
                                     class="modal fade" tabindex="-1" role="dialog"
                                     aria-labelledby="myModalLabel" aria-hidden="true"
                                     style="display: none;">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">
                                                    Buy Shares
                                                </h4>
                                                <button type="button" class="close"
                                                        data-dismiss="modal"
                                                        aria-hidden="true">×
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label>
                                                        Number of Shares
                                                    </label>
                                                    <input type="number" class="form-control" name="shares_no" required>
                                                    <input type="hidden" name="user_id" value="<?php echo $user_id ;?>">
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button"
                                                        class="btn btn-default waves-effect btn-sm"
                                                        data-dismiss="modal">Close
                                                </button>

                                                <button type="submit"
                                                        class="btn btn-danger waves-effect waves-light btn-sm">
                                                    Purchase
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <hr>
                        </div>

                        <div class="col-sm-6">
                            <h6>
                                <b>
                                    Monthly Contributions
                                </b>
                            </h6>
                            <hr>
                            <h6>
                                <i class="fa fa-bookmark"></i>
                                &nbsp;
                                No. of Months
                                <b>
                                    &nbsp;
                                    <?php echo $details[8]['contrib_months']?> &nbsp;Month(s)
                                </b>
                            </h6>
                            <h6>
                                <i class="fa fa-money"></i>
                                &nbsp;
                                Total Contributions
                                <b>
                                    &nbsp;
                                    Ksh. &nbsp;<?php echo number_format($details[9]['total_contribs']) ;?>
                                </b>
                            </h6>
                            <br>
                            <button class="btn btn btn-danger btn-sm" data-toggle="modal" data-target="#contribModal">
                                Monthly Contribution
                            </button>
                            <form action="<?php echo base_url('payments/buy_shares')?>" method="post" id="buy_shares_form">
                                <div id="contribModal"
                                     class="modal fade" tabindex="-1" role="dialog"
                                     aria-labelledby="myModalLabel" aria-hidden="true"
                                     style="display: none;">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">
                                                    Information
                                                </h4>
                                                <button type="button" class="close"
                                                        data-dismiss="modal"
                                                        aria-hidden="true">×
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <h6>
                                                    A monthly contribution of &nbsp;
                                                    <b>
                                                        Ksh.&nbsp;
                                                        <?php

                                                        if($details[5]['type_id'] == 'terabyte')
                                                        {
                                                            echo number_format(5000);
                                                        }

                                                        if($details[5]['type_id'] == 'petabyte')
                                                        {
                                                            echo number_format(15000);
                                                        }

                                                        if($details[5]['type_id'] == 'gigabyte')
                                                        {
                                                            echo number_format(1000);
                                                        }

                                                        if($details[5]['type_id'] == 'megabyte')
                                                        {
                                                            echo number_format(250);
                                                        }

                                                        if($details[5]['type_id'] == 'kilobyte')
                                                        {
                                                            echo number_format(500);
                                                        }

                                                        ?>
                                                    </b>
                                                    &nbsp;
                                                    is deducted every month automatically
                                                </h6>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button"
                                                        class="btn btn-default waves-effect btn-sm"
                                                        data-dismiss="modal">Close
                                                </button>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <hr>
                        </div>
                    </div>
                    <br>
                    <h6>
                        <b>
                            Recent Activities
                        </b>
                    </h6>
                    <hr>
                    <div class="row">
                        <div class="col-12">
                            <div style="height: 30vh; overflow: auto;">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>Description</th>
                                        <th>Amount</th>
                                        <th>Date</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($activities as $activity) {?>
                                        <tr>
                                            <td>
                                                <?php echo ucwords(strtolower($activity->activity), " ") ;?>
                                            </td>
                                            <td>
                                                Ksh. <?php echo number_format($activity->amount)?>
                                            </td>
                                            <td>
                                                <?php echo date_format(date_create($activity->date), 'jS M, Y') ;?>
                                            </td>
                                        </tr>
                                    <?php }?>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Description</th>
                                        <th>Amount</th>
                                        <th>Date</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url('assets/external/js/jquery.js') ;?>"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#top_up_wallet_form").submit(function (e) {
            e.preventDefault();

            $.ajax({
                type: "POST",
                url: $(this).attr("action"),
                data: $(this).serializeArray(),
                beforeSend: function () {
                    swal({
                        title: "Please Wait",
                        text: "Processing Mpesa payment request...",
                        type: "info",
                        showConfirmButton: false,
                        showCancelButton: false
                    });
                },

                success: function (res) {

                    if(res === '0')
                    {
                        swal({
                            title: "Process Payment",
                            text: "Please accept the Mpesa payment request on your phone to complete your top up",
                            type: "info",
                            showConfirmButton: false,
                            showCancelButton: false
                        });

                        setTimeout(function () {
                            window.location.reload();
                        }, 5000);
                    }

                    else
                    {
                        swal({
                            title: "Processing Error",
                            text: "Payment Request Failed. Try again after some time or refresh this page.",
                            type: "warning",
                            showConfirmButton: true,
                            confirmButtonText: "Ok",
                            confirmButtonColor: ok_btn_color,
                            showCancelButton: false
                        });
                    }
                }
            });
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#buy_shares_form").submit(function (e) {
            e.preventDefault();

            $.ajax({
                type: "POST",
                url: $(this).attr("action"),
                data: $(this).serializeArray(),
                beforeSend: function () {
                    swal({
                        title: "Please Wait",
                        text: "Processing shares payment...",
                        type: "info",
                        showConfirmButton: false,
                        showCancelButton: false
                    });
                },

                success: function (res) {

                    if(res === 'no_funds')
                    {
                        swal({
                            title: "Insufficient Funds",
                            text: "Please top up your wallet first.",
                            type: "success",
                            showConfirmButton: true,
                            confirmButtonText: "Ok",
                            confirmButtonColor: ok_btn_color,
                            showCancelButton: false
                        });
                    }

                    else if(res === 'not_paid')
                    {
                        swal({
                            title: "Registration Not Paid",
                            text: "Please pay your registration fee first.",
                            type: "warning",
                            showConfirmButton: true,
                            confirmButtonText: "Ok",
                            confirmButtonColor: ok_btn_color,
                            showCancelButton: false
                        });
                    }

                    else
                    {
                        swal({
                            title: "Successful",
                            text: "Your purchase has been processed successfully .Reloading ...",
                            type: "success",
                            showConfirmButton: false,
                            showCancelButton: false
                        });

                        setTimeout(function () {
                            window.location.reload();
                        }, 2000)
                    }
                }
            });
        });
    });
</script>

<script type="text/javascript">
    (function fetch_user_wallet_amount() {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('auth/user_wallet_amount/'.$user_id)?>",
            success: function (res) {
                $("#user_wallet_div").html(res);
            },
            complete: function () {
                setTimeout(fetch_user_wallet_amount, 1000);
            }
        });
    })();
</script>
