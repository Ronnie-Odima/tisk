
<nav class="navbar navbar-expand-lg navbar-expand bg-danger">
    <div class="container">
        <div class="col-12">
            <div class="text-center">
                <div style="width: 150px; height: 150px; display: block; margin-left: auto; margin-right: auto;">
                    <img src="<?php echo base_url().$details[1]['image']?>" class="img-fluid" style="width: 100%; height: 100%;" />
                </div>
                <br>
                <h4 style="color: white;">
                    <b>
                        <?php echo ucwords(strtolower($details[0]['name']), " ") ;?>
                    </b>
                </h4>
                <h6 style="color: white;">
                    (<b>
                        <?php echo ucwords(strtolower($details[5]['type_id']), " ") ;?> &nbsp; Account
                    </b>)
                </h6>
                <a type="button" href="<?php echo base_url('user/logout')?>" class="btn btn-outline-dark btn-sm">
                    <i class="fa fa-lock"></i>
                    &nbsp;
                    Logout
                </a>
            </div>
        </div>
    </div>
</nav>
<br>