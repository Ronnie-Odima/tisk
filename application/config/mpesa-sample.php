<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * MPESA API (**DO NOT EDIT**)
 */
$config['live_url'] = "https://api.safaricom.co.ke/mpesa/";
$config['sandbox_url'] = "https://sandbox.safaricom.co.ke/mpesa/";

/**
 * MPESA Oauth Token generator URL (**DO NOT EDIT**)
 */
$config['live_token_url'] = "https://api.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials";
$config['sandbox_token_url'] = "https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials";

/**
 * MPESA API credentials(**EDIT VALUES**)
 */

$config['consumer_key'] = "ffjU3KHn6KGYnQfhLRUdMhziVz62CckG";
$config['consumer_secret'] = "9oZbw4tenlxL8ezu";
$config['pass_key'] = "5f0455715bf3d5341f6286afd4a5b01ffc83d648a7910c68cd8d9127ed04729f";
$config['callback'] = base_url()."payments/stkCallBack";
$config['short_code'] = "606172";

/**
 * APP settings
 */
$config['application_status'] = "live"; //values: live, sandbox


