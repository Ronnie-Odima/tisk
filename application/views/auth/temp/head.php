<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>
        <?php echo $title ;?>
    </title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700,900,400italic,600italic,700italic|Varela+Round&subset=latin,latin-ext" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css')?>">

    <?php if($page == 'login') {?>
        <link rel="stylesheet" href="<?php echo base_url('assets/external/css/login.css')?>">
    <?php }?>

    <?php if($page == 'signup') {?>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/external/css/reg.css')?>">
    <?php }?>

    <?php if($page == '') {?>
        <link rel="stylesheet" href="<?php echo base_url('assets/external/css/welcome.css')?>">
    <?php }?>

    <?php if($page == 'category') {?>
        <link rel="stylesheet" href="<?php echo base_url('assets/external/css/category.css')?>">
    <?php }?>

    <link rel="stylesheet" href="<?php echo base_url('assets/external/sweetalert2/sweetalert2.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/external/css/custom.css')?>">
</head>
<body>

