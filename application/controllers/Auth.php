<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller
{
    public function index()
    {
        /*$test = 1 + 1;

        $expected_result = 2;

        $test_name = 'Adds one plus one';

        $this->unit->run($test, $expected_result, $test_name);

        echo $this->unit->report();*/

        $data = array
        (
            'title' => 'Tisk:: Welcome',
            'page' => $this->uri->segment(2)
        );

        $this->load->view('auth/temp/head', $data);
        $this->load->view('auth/pages/welcome', $data);
        $this->load->view('auth/temp/scripts', $data);
    }

    public function login()
    {
        $data = array
        (
            'title' => 'Tisk:: Login',
            'page' => $this->uri->segment(2)
        );

        $this->load->view('auth/temp/head', $data);
        $this->load->view('auth/pages/login', $data);
        $this->load->view('auth/temp/scripts', $data);

    }

    public function category()
    {
        $data = array
        (
            'title' => 'Tisk:: Category',
            'page' => $this->uri->segment(2)
        );

        $this->load->view('auth/temp/head', $data);
        $this->load->view('auth/pages/category', $data);
        $this->load->view('auth/temp/scripts', $data);
    }

    public function signup()
    {
        $data = array
        (
            'title' => 'Tisk:: Sign Up',
            'page' => $this->uri->segment(2),
            'type' => $this->uri->segment(3),
            'counties' => $this->General->get_counties()
        );

        $this->load->view('auth/temp/head', $data);
        $this->load->view('auth/pages/signup', $data);
        $this->load->view('auth/temp/scripts', $data);
    }

    public function reg_action()
    {
        $type = $this->uri->segment(3);

        $sal = $this->input->post('salutation');
        $fname = $this->input->post('first_name');
        $lname = $this->input->post('last_name');
        $email = $this->input->post('email');
        $phone = "254".$this->input->post('phone_number');
        $n_id= $this->input->post('national_id');
        $dob = $this->input->post('dob');
        $gender = $this->input->post('gender');
        $location = $this->input->post('location');
        $pass1 = $this->input->post('password');
        $pass2 = $this->input->post('password2');

        $ent_name = $this->input->post('name');
        $org_type = $this->input->post('org_type');
        $other_org_type = $this->input->post('other_type');
        $reg_no = $this->input->post('reg_no');
        $reg_date = $this->input->post('reg_date');
        $reg_office = $this->input->post('reg_office');
        $postal_add =  $this->input->post('postal_add');
        $physical_add =  $this->input->post('physical_add');
        $office_tel =  $this->input->post('office_tel');
        $office_email =  $this->input->post('office_email');
        $c_p_name =  $this->input->post('c_p_name');
        $c_p_phone =  "254".$this->input->post('c_p_phone');
        $b_nature =  $this->input->post('b_nature');
        $pass_corp_1 =  $this->input->post('password');
        $pass_corp_2 =  $this->input->post('password2');

        $corp_type = null;

        if ($org_type == 'other')
        {
            $corp_type = $other_org_type;
        }
        else
        {
            $corp_type = $org_type;
        }



        if($type == 'individual')
        {
            $user_id = uniqid(true);

            $image__user = "./assets/images/image.png";

            $individual_data = array
            (
                'user_id' => $user_id,
                'fname' =>$fname,
                'lname' => $lname,
                'salutation' => $sal,
                'email' => $email,
                'phone' => $phone,
                'n_id' => $n_id,
                'dob' => $dob,
                'gender' => $gender,
                'location' => $location,
                'image' => $image__user
            );

            $login_data = array
            (
                'user_id' => $user_id,
                'email' => $email,
                'password' => sha1($pass1),
                'level' => 2
            );

            $email_exists = $this->General->check_email_users($email);
            $phone_exists = $this->General->check_phone_users($phone);

            if($pass1 != $pass2)
            {
                echo "pass_mismatch";

                exit(0);
            }

            elseif($email_exists > 0)
            {
                echo "email_exists";

                exit(0);
            }

            elseif($phone_exists > 0)
            {
                echo "phone_exists";

                exit(0);
            }

            else
            {
                $this->db->insert('users', $individual_data);
                $this->db->insert('logins', $login_data);

                $u_id = $this->db->get_where('users', array('user_id' => $user_id))->row('id');
                $user_acc_no = $fname."-".$lname."-TISK-".$u_id;

                $this->db->where('user_id', $user_id);
                $this->db->set('acc_no', $user_acc_no);
                $this->db->update('users');

                $link_indiv = base_url()."auth/dash_confirm/".$user_id."/individual/".md5(uniqid())."/".md5(uniqid());

                $msg2 = "Hello $fname $lname, Welcome to TISK. Click the link below to continue <br /><br /> $link_indiv";
                $sub2 = "Registration successful";

                $this->General->email($msg2, $sub2, $email);

                echo "individual";

                exit(0);
            }
        }

        if($type == 'corporate')
        {
            $corporate_id = uniqid(true);

            $image__corp = "./assets/images/corp.png";

            $corporation_data = array
            (
                'corporate_id' => $corporate_id,
                'name' => $ent_name,
                'type' => $corp_type,
                'reg_no' => $reg_no,
                'reg_date' => $reg_date,
                'reg_office' => $reg_office,
                'postal_add' => $postal_add,
                'physical_add' => $physical_add,
                'office_tel' => $office_tel,
                'office_email' => $office_email,
                'c_p_name' => $c_p_name,
                'c_p_phone' => $c_p_phone,
                'nature' => $b_nature,
                'image' => $image__corp
            );

            $login_corp_data = array
            (
                'user_id' => $corporate_id,
                'email' => $office_email,
                'password' => sha1($pass_corp_1),
                'level' => 3
            );

            $email_exists_corp = $this->General->check_email_users_corp($office_email);
            $phone_exists_corp = $this->General->check_phone_users_corp($office_tel);
            $name_exists = $this->General->check_name_users_corp($ent_name);

            if($pass_corp_1 != $pass_corp_2)
            {
                echo "pass_mismatch";

                exit(0);
            }

            elseif($name_exists > 0)
            {
                echo "name_exists";

                exit(0);
            }


            elseif($email_exists_corp > 0)
            {
                echo "email_exists";

                exit(0);
            }

            elseif($phone_exists_corp > 0)
            {

                echo "phone_exists";

                exit(0);
            }

            else
            {
                $this->db->insert('corporates', $corporation_data);
                $this->db->insert('logins', $login_corp_data);

                $corp_id = $this->db->get_where('corporates', array('corporate_id' => $corporate_id))->row('id');
                $corp_acc_no = $ent_name."-TISK-".$corp_id;

                $this->db->where('corporate_id', $corporate_id);
                $this->db->set('acc_no', $corp_acc_no);
                $this->db->update('corporates');

                $link_corp = base_url()."auth/dash_confirm/".$corporate_id."/corporate/".md5(uniqid())."/".md5(uniqid());
                $msg = "Hello $ent_name, Welcome to TISK. Click the link below to continue <br /><br /> $link_corp";
                $sub = "Registration successful";

                $this->General->email($msg, $sub, $office_email);

                echo "corporate";

                exit(0);
            }
        }


    }


    public function login_action()
    {
        $email = $this->input->post('email');
        $password = sha1($this->input->post('password'));

        $login = $this->db->get_where('logins', array('email' => $email, 'password' => $password));

        $has_account = $this->General->check_user_has_account($login->row('user_id'));

        if($login->num_rows() == 1)
        {
            if($has_account != null)
            {
                $sess_data = array
                (
                    'user_id' => $login->row('user_id')
                );

                $this->session->set_userdata('user_session', $sess_data);

                redirect('user/dashboard');
            }
            else
            {
                redirect('auth/login?no_account=true');
            }

        }
        else
        {
            redirect('auth/login?error=user');
        }
    }

    public function login_action_direct($email, $password)
    {
        $login = $this->db->get_where('logins', array('email' => $email, 'password' => $password));

        if($login->num_rows() == 1)
        {
            $sess_data = array
            (
                'user_id' => $login->row('user_id')
            );

            $this->session->set_userdata('user_session', $sess_data);

            redirect('user/dashboard');
        }
        else
        {
            redirect('auth/login?error=user');
        }
    }

    public function login_redirect()
    {
        $user_id = $this->uri->segment(3);
        $type = $this->uri->segment(4);

        if($type == 'corporate')
        {
            $email = $this->db->get_where('logins', array('user_id' => $user_id))->row('email');
            $pass = $this->db->get_where('logins', array('user_id' => $user_id))->row('password');

            $this->login_action_direct($email, $pass);
        }

        if($type == 'individual')
        {
            $email = $this->db->get_where('logins', array('user_id' => $user_id))->row('email');
            $pass = $this->db->get_where('logins', array('user_id' => $user_id))->row('password');

            $this->login_action_direct($email, $pass);
        }

    }


    public function dash_confirm()
    {
        $data = array
        (
            'title' => 'Tisk:: Configure Account',
            'page' => $this->uri->segment(2),
            'id' => $this->uri->segment(3),
            'details' => $this->General->get_user_information($this->uri->segment(3))
        );

        $this->load->view('auth/temp/head', $data);
        $this->load->view('dashboards//dash0', $data);
        $this->load->view('dashboards//dash0_page', $data);
        $this->load->view('auth/temp/scripts', $data);
    }

    public function user_wallet_amount()
    {
        $user_id = $this->uri->segment(3);

        $amount = $this->General->get_wallet_amount($user_id);

        echo

        '
        
                        <h5>
                            <i class="fa fa-money"></i>
                            &nbsp;
                            <b>
                                KSH. '.number_format($amount).'
                            </b>
                        </h5>
        
        
        ';
    }

    public function check_reg_fee_paid()
    {
        $user_id = $this->uri->segment(3);

        $paid = $this->General->check_user_reg_fee_paid($user_id);

        if($paid > 0)
        {
            echo "paid";
        }

        else
        {
            echo "not_paid";
        }
    }

    public function check_min_shares_bought()
    {
        $user_id = $this->uri->segment(3);

        $no_shares = $this->General->check_user_min_shares($user_id);

        if($no_shares >= 4)
        {
            echo "min_passed";
        }

        else
        {
            echo "min_failed";
        }
    }

    public function open_account()
    {
        $user_id = $this->uri->segment(3);
        $type = $this->uri->segment(4);
        $account = $this->uri->segment(5);
        $account_amount = 0;
        $date = date('Y-m-d');
        $period = 1;
        $start_date = strtotime(date("Y-m-d", strtotime($date)));
        $next_date = strtotime(date("Y-m-d", strtotime($date)) . " +$period month");

        if($account == 'terabyte')
        {
            $account_amount = 5000;
        }

        if($account == 'petabyte')
        {
            $account_amount = 15000;
        }

        if($account == 'gigabyte')
        {
            $account_amount =  1000;
        }

        if($account == 'megabyte')
        {
            $account_amount =  250;
        }

        if($account == 'kilobyte')
        {
            $account_amount = 500;
        }

        $shares_bought = $this->General->check_user_min_shares($user_id);

        if($shares_bought >= 4)
        {
            if($type == 'corporate')
            {
                $this->db->where('corporate_id', $user_id);
                $this->db->set('type_id', $account);
                $this->db->update('corporates');

                $contrib_track_data = array
                (
                    'user_id' => $user_id,
                    'amount' => $account_amount,
                    'start_month' => date('Y-m-d', $start_date),
                    'next_month' => date('Y-m-d', $next_date)

                );

                $exists = $this->db->get_where('contributons_track', array('user_id' => $user_id))->num_rows();

                if($exists > 0)
                {
                    $this->db->where('user_id', $user_id);
                    $this->db->update('contributons_track', $contrib_track_data);

                    echo $user_id."/corporate";

                    exit(0);
                }
                else
                {
                    $this->db->insert('contributons_track', $contrib_track_data);

                    echo $user_id."/corporate";

                    exit(0);
                }

            }

            elseif($type == 'individual')
            {
                $this->db->where('user_id', $user_id);
                $this->db->set('type_id', $account);
                $this->db->update('users');

                $contrib_track_data = array
                (
                    'user_id' => $user_id,
                    'amount' => $account_amount,
                    'start_month' => date('Y-m-d', $start_date),
                    'next_month' => date('Y-m-d', $next_date)

                );

                $exists = $this->db->get_where('contributons_track', array('user_id' => $user_id))->num_rows();

                if($exists > 0)
                {
                    $this->db->where('user_id', $user_id);
                    $this->db->update('contributons_track', $contrib_track_data);

                    echo $user_id."/individual";

                    exit(0);
                }
                else
                {
                    $this->db->insert('contributons_track', $contrib_track_data);

                    echo $user_id."/individual";

                    exit(0);
                }
            }
            else
            {
                echo "error";

                exit(0);
            }
        }
        else
        {
            echo "no_shares";

            exit(0);
        }

    }

}


