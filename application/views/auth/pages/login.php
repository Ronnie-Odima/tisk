
<div class="signInGrid">
    <div class="formGrid">
        <div class="headerGrid"></div>

        <form action="<?php echo base_url('auth/login_action')?>" method="post">
            <div class="form">
                <?php if(isset($_GET['error'])) {?>
                    <div class="alert alert-danger">
                        Invalid user credentials
                    </div>
                <?php }?>
                <?php if(isset($_GET['no_account'])) {?>
                    <div class="alert alert-warning">
                        Please check your email address for account creation link
                    </div>
                <?php }?>
                <div class="formGroup">
                    <div class="inputField">
                        <input
                                type="email"
                                id="email"
                                name="email"
                                placeholder="email"
                        />
                    </div>
                    <div class="inputField">
                        <input
                                type="password"
                                id="password"
                                name="password"
                                placeholder="password"
                        />
                    </div>
                </div>
            </div>
            <div class="formSubmitGroup">
                <button style="background: #b32017; color: #ffffff;" class="formSubmitButton" type="submit">
                    Sign In
                </button>
            </div>
        </form>
        <p class="signUpText">
            <br>
            Don't have an account?
            <a href="<?php echo base_url('auth/category')?>">
                 Sign Up
            </a>
        </p>
    </div>
</div>