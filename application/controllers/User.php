<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller
{
    public function dashboard()
    {
        $session = $this->check_session();

        $data = array
        (
            'title' => 'Tisk:: User Dashboard',
            'page' => $this->uri->segment(2),
            'user_id' => $session,
            'details' => $this->General->get_user_information($session),
            'activities' => $this->General->get_user_activities($session)
        );

        $this->load->view('auth/temp/head', $data);
        $this->load->view('dashboards//dash1', $data);
        $this->load->view('dashboards//dash1_page', $data);
        $this->load->view('auth/temp/scripts', $data);
    }

    public function check_session()
    {
        $session = $this->session->userdata('user_session');

        if ($session) {
            return $session['user_id'];
        } else {
            redirect('auth');
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('user_session');

        redirect('auth');
    }

}