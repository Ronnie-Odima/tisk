<?php if($type == 'individual') {?>
<form action="<?php echo base_url('auth/reg_action/individual')?>" method="post" id="reg_action_form">
    <div class="signUpGrid">
        <div class="formGrid">
            <div class="headerGrid"></div>
            <div class="form">
                <div class="formGroup">
                    <div class="inputField">
                        <select name="salutation" required>
                            <option value="">--- Add Salutation ---</option>
                            <option value="Mr.">Mr.</option>
                            <option value="Mrs.">Mrs.</option>
                            <option value="Miss">Miss</option>
                        </select>
                    </div>
                    <div class="inputField">
                        <input
                                type="text"
                                id="first_name"
                                name="first_name"
                                placeholder="First name"
                        />
                    </div>
                    <div class="inputField">
                        <input
                                type="text"
                                id="last_name"
                                name="last_name"
                                placeholder="Last name"
                        />
                    </div>
                    <div class="inputField">
                        <input
                                type="email"
                                id="email"
                                name="email"
                                placeholder="Email"
                        />
                    </div>
                    <div class="inputField input-group prefix">
                        <span class="input-group-addon">+254</span>

                        <input
                                type="tel"
                                id="phoneNumber"
                                name="phone_number"
                                placeholder="Phone number"
                        />
                    </div>
                    <div class="inputField">
                        <input
                                type="number"
                                min="8"
                                id="national_id"
                                name="national_id"
                                placeholder="National ID"
                        />
                    </div>
                    <div class="inputField">
                        <select name="gender" required>
                            <option value="">--- Add Gender ---</option>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                        </select>
                    </div>
                    <div class="inputField input-group prefix">
                        <span class="input-group-addon">Date of Birth</span>

                        <input
                                type="text"
                                id="date_field"
                                name="dob"
                                required
                        />
                    </div>
                    <div class="inputField">
                        <select name="location" required>
                            <option value="">--- Add Location ---</option>
                            <?php foreach ($counties as $county) {?>
                                <option value="<?php echo ucwords(strtolower($county->name), " ")?>">
                                    <?php echo ucwords(strtolower($county->name), " ")?>
                                </option>
                            <?php }?>
                        </select>
                    </div>
                    <div class="inputField">
                        <input
                                type="password"
                                id="password"
                                name="password"
                                placeholder="Password"
                        />
                    </div>
                    <div class="inputField">
                        <input
                                type="password"
                                id="password2"
                                name="password2"
                                placeholder="Confirm password"
                        />
                    </div>
                </div>
            </div>
            <div class="formSubmitGroup">
                <button style="background: #b32017; color: #ffffff;" class="formSubmitButton" type="submit">
                    Sign In
                </button>
            </div>
        </div>
    </div>
</form>

    <p class="signInText">
        already have an account? <a href="<?php echo base_url('auth/login')?>">sign in</a>
    </p>


<?php }?>

<?php if($type == 'corporate') {?>
    <form action="<?php echo base_url('auth/reg_action/corporate')?>" method="post" id="reg_action_form_corp">
        <div class="signUpGrid">
            <div class="formGrid">
                <div class="headerGrid"></div>
                <div class="form">
                    <div id="otherModal" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-body">
                                   <label>
                                       Enter Organization Type
                                   </label>
                                    <input type="text" class="form-control" name="other_type">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="formGroup">
                        <div class="inputField">
                            <input
                                    type="text"
                                    id="name"
                                    name="name"
                                    placeholder="Entity name"
                            />
                        </div>
                        <div class="inputField">
                            <label>
                                Type of Organization
                            </label>
                            <br>
                            <label class="radio-inline">
                                <input type="radio" name="org_type" value="company" checked>&nbsp;Company
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="org_type" value="co-operative">&nbsp;Co-operative
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="org_type" value="partnership">&nbsp;Partnership
                            </label>
                            <label class="radio-inline" id="other_org">
                                <input type="radio" name="org_type" value="other">&nbsp;Other
                            </label>
                        </div>
                        <div class="inputField">
                            <input
                                    type="text"
                                    id="reg_no"
                                    name="reg_no"
                                    placeholder="Registration Number"
                            />
                        </div>

                        <div class="inputField">
                            <input
                                    type="text"
                                    id="date_field_corp"
                                    name="reg_date"
                                    placeholder="Date of Registration"
                                    required
                            />
                        </div>

                        <div class="inputField">
                            <input
                                    type="text"
                                    id="reg_office"
                                    name="reg_office"
                                    placeholder="Registration Office"
                            />
                        </div>

                        <div class="inputField">
                            <input
                                    type="text"
                                    id="postal_add"
                                    name="postal_add"
                                    placeholder="Postal Address"
                            />
                        </div>

                        <div class="inputField">
                            <input
                                    type="text"
                                    id="physical_add"
                                    name="physical_add"
                                    placeholder="Physical Address"
                            />
                        </div>

                        <div class="inputField">

                            <input
                                    type="tel"
                                    id="office_tel"
                                    name="office_tel"
                                    placeholder="Office Telephone"
                            />
                        </div>

                        <div class="inputField">

                            <input
                                    type="email"
                                    id="office_email"
                                    name="office_email"
                                    placeholder="Office Email"
                            />
                        </div>

                        <div class="inputField">

                            <input
                                    type="text"
                                    id="c_p_name"
                                    name="c_p_name"
                                    placeholder="Contact Person Name"
                            />
                        </div>

                        <div class="inputField input-group prefix">
                            <span class="input-group-addon">+254</span>

                            <input
                                    type="tel"
                                    id="c_p_phone"
                                    name="c_p_phone"
                                    placeholder="contact Person's Phone"
                            />
                        </div>
                        <div class="inputField">
                            <input
                                    type="text"
                                    id="b_nature"
                                    name="b_nature"
                                    placeholder="Nature of Business"
                            />
                        </div>
                        <div class="inputField">
                            <input
                                    type="password"
                                    id="password_corp"
                                    name="password"
                                    placeholder="Password"
                            />
                        </div>
                        <div class="inputField">
                            <input
                                    type="password"
                                    id="password_corp2"
                                    name="password2"
                                    placeholder="Confirm password"
                            />
                        </div>
                    </div>
                </div>

                <div class="formSubmitGroup">
                    <button style="background: #b32017; color: #ffffff;" class="formSubmitButton" type="submit">
                        Sign In
                    </button>
                </div>
                <p class="signInText">
                    already have an account? <a href="<?php echo base_url('auth/login')?>">sign in</a>
                </p>
            </div>
        </div>
    </form>

<?php }?>

