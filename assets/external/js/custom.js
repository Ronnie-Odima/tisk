var base_url = "http://localhost/CI/tisk_web/";
var ok_btn_color = "#b32017";

$(document).ready(function () {
    $("#reg_action_form").submit(function (e) {
        e.preventDefault();

        $.ajax({
            type: "POST",
            url: $(this).attr("action"),
            data: $(this).serializeArray(),
            beforeSend: function () {
                swal({
                    title: "Please Wait",
                    text: "Processing registration details...",
                    type: "info",
                    showConfirmButton: false,
                    showCancelButton: false
                });
            },

            success: function (res) {

                if(res === 'pass_mismatch')
                {
                    swal({
                        title: "Password Error",
                        text: "Password enter do not match confirmation password",
                        type: "warning",
                        showConfirmButton: true,
                        confirmButtonText: "Ok",
                        confirmButtonColor: ok_btn_color,
                        showCancelButton: false
                    });
                }

                if(res === 'email_exists')
                {
                    swal({
                        title: "Email Exists",
                        text: "Email address exists. Please use another email address",
                        type: "warning",
                        showConfirmButton: true,
                        confirmButtonText: "Ok",
                        confirmButtonColor: ok_btn_color,
                        showCancelButton: false
                    });
                }

                if(res === 'Phone_exists')
                {
                    swal({
                        title: "Phone Exists",
                        text: "Phone number exists. Please use another phone number",
                        type: "warning",
                        showConfirmButton: true,
                        confirmButtonText: "Ok",
                        confirmButtonColor: ok_btn_color,
                        showCancelButton: false
                    });
                }

                if(res === 'individual')
                {
                    swal({
                        title: "Successful",
                        text: "Your data has been saved. Please check your Email Address for more information",
                        type: "success",
                        showConfirmButton: false,
                        showCancelButton: false
                    });

                    setTimeout(function () {
                        window.location.reload();
                    }, 3000);
                }
            }
        });
    });
});


$(document).ready(function () {
    $("#reg_action_form_corp").submit(function (e) {
        e.preventDefault();

        $.ajax({
            type: "POST",
            url: $(this).attr("action"),
            data: $(this).serializeArray(),
            beforeSend: function () {
                swal({
                    title: "Please Wait",
                    text: "Processing registration details...",
                    type: "info",
                    showConfirmButton: false,
                    showCancelButton: false
                });
            },

            success: function (res) {

                if(res === 'pass_mismatch')
                {
                    swal({
                        title: "Password Error",
                        text: "Password enter do not match confirmation password",
                        type: "warning",
                        showConfirmButton: true,
                        confirmButtonText: "Ok",
                        confirmButtonColor: ok_btn_color,
                        showCancelButton: false
                    });
                }

                if(res === 'name_exists')
                {
                    swal({
                        title: "Name Exists",
                        text: "Organization with such name already Exists.",
                        type: "warning",
                        showConfirmButton: true,
                        confirmButtonText: "Ok",
                        confirmButtonColor: ok_btn_color,
                        showCancelButton: false
                    });
                }

                if(res === 'email_exists')
                {
                    swal({
                        title: "Email Exists",
                        text: "Email address exists. Please use another email address",
                        type: "warning",
                        showConfirmButton: true,
                        confirmButtonText: "Ok",
                        confirmButtonColor: ok_btn_color,
                        showCancelButton: false
                    });
                }

                if(res === 'phone_exists')
                {
                    swal({
                        title: "Phone Exists",
                        text: "Office telephone exists. Please use another telephone number",
                        type: "warning",
                        showConfirmButton: true,
                        confirmButtonText: "Ok",
                        confirmButtonColor: ok_btn_color,
                        showCancelButton: false
                    });
                }

                if(res === 'corporate')
                {
                    swal({
                        title: "Successful",
                        text: "Your data has been saved. Please check your Email Address for more information",
                        type: "success",
                        showConfirmButton: false,
                        showCancelButton: false
                    });

                    setTimeout(function () {
                        window.location.reload();
                    }, 3000);
                }
            }
        });
    });
});

$(document).ready(function () {
    $('#other_org').click(function() {
        $('#otherModal').modal('show');
    });
});