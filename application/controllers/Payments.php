<?php
/**
 * Created by PhpStorm.
 * User: ronnie-odima
 * Date: 8/13/18
 * Time: 12:11 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Payments extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('mpesa');
    }

    public function stkPush()
    {
        $amount = $this->input->post('amount');
        $phone = $this->input->post('phone');
        $acc_no = $this->input->post('acc_no');
        $dec = "wallet_top_up";
        $type = "CustomerPayBillOnline";

        $result = $this->mpesa->STKPushSimulation($type, $amount, $phone, $phone, $acc_no, $dec, "Remarks");

        echo $result->ResponseCode;
    }

    public function stkCallBack()
    {
        $callbackJSONData = file_get_contents('php://input');
        $callbackData = json_decode($callbackJSONData, true);
        $resultCode = $callbackData['Body']['stkCallback']['ResultCode'];
        $resultDesc = $callbackData['Body']['stkCallback']['ResultDesc'];
        $merchantRequestID = $callbackData['Body']['stkCallback']['MerchantRequestID'];
        $checkoutRequestID = $callbackData['Body']['stkCallback']['CheckoutRequestID'];
        $amount = $callbackData['Body']['stkCallback']['CallbackMetadata']['Item'][0]['Value'] ;
        $mpesaReceiptNumber = $callbackData['Body']['stkCallback']['CallbackMetadata']['Item'][1]['Value'];
        $transactionDate = $callbackData['Body']['stkCallback']['CallbackMetadata']['Item'][3]['Value'];
        $phoneNumber = $callbackData['Body']['stkCallback']['CallbackMetadata']['Item'][4]['Value'];
        $trans_type = "M-Pesa";

        if($resultCode == "0" && $mpesaReceiptNumber != '')
        {
            $trans_id = uniqid(true);

            $stk_data = array
            (
                'trans_id' => $trans_id,
                'mpesa_r_no' => $mpesaReceiptNumber,
                'merchant_id' => $merchantRequestID,
                'checkout_id' => $checkoutRequestID,
                'phone' => $phoneNumber,
                'amount' => $amount,
                'description' => $resultDesc,
                'trans_date' => $transactionDate
            );

            $this->db->insert('stk_trans', $stk_data);

            $user_id = $this->General->get_user_id($phoneNumber);

            $this->General->update_wallet($user_id, $amount, $trans_type, $mpesaReceiptNumber);

            $activity_data = array
            (
                'user_id' => $user_id,
                'activity' => "Wallet Top up",
                'amount' => $amount
            );

            $this->db->insert('activities', $activity_data);
        }

        else
        {
            echo "cancelled";
        }
    }

    public function pay_reg_fee()
    {

        $user_id = $this->uri->segment(3);
        $wallet_amount = $this->General->get_wallet_amount($user_id);

        if($wallet_amount < 1000)
        {
            echo "no_funds";

            exit(0);
        }
        else
        {
            $reg_fee_data = array
            (
                'user_id' => $user_id,
                'paid' => 1
            );

            $this->db->insert('user_reg_fee', $reg_fee_data);

            $new_wallet = $wallet_amount - 1000;
            $this->General->update_wallet_direct($user_id, $new_wallet);

            $activity_data = array
            (
                'user_id' => $user_id,
                'activity' => "Paid Registration Fee",
                'amount' => 1000
            );

            $this->db->insert('activities', $activity_data);

            echo "reg_paid";

            exit(0);
        }

    }

    public function buy_shares()
    {

        $user_id = $this->input->post('user_id');
        $shares_no = $this->input->post('shares_no');
        $wallet_amount = $this->General->get_wallet_amount($user_id);

        $total_shares_amount = $shares_no * 250;

        $paid_reg = $this->General->check_user_reg_fee_paid($user_id);

        if($paid_reg > 0)
        {
            if($wallet_amount < $total_shares_amount)
            {
                echo "no_funds";

                exit(0);
            }
            else
            {
                $shares_data = array
                (
                    'user_id' => $user_id,
                    'no_shares' => $shares_no,
                    'amount' => $total_shares_amount
                );

                $this->db->insert('user_shares', $shares_data);

                $new_wallet = $wallet_amount - $total_shares_amount;
                $this->General->update_wallet_direct($user_id, $new_wallet);

                $activity_data = array
                (
                    'user_id' => $user_id,
                    'activity' => "Bought Shares",
                    'amount' => $total_shares_amount
                );

                $this->db->insert('activities', $activity_data);

                echo "shares_paid";

                exit(0);
            }
        }

        else
        {
            echo "not_paid";
        }
    }

    public function pay_monthly_contributions()
    {
        $today = date('Y-m-d');

        $start_date = $this->db->get_where('contributons_track', array('start_month' => $today));
        $next_date = $this->db->get_where('contributons_track', array('next_month' => $today));

        if($start_date->num_rows() > 0)
        {
            foreach ($start_date->result() as $value)
            {
                $wallet_amount = $this->General->get_wallet_amount($value->user_id);

                if($wallet_amount > $value->amount)
                {
                    $conrib_data = array
                    (
                        'user_id' => $value->user_id,
                        'amount' => $value->amount,
                        'month' => date('m'),
                        'year' => date('Y')
                    );

                    $this->db->insert('contributions', $conrib_data);

                    $new_wallet = $wallet_amount - $value->amount;

                    $this->General->update_wallet_direct($value->user_id, $new_wallet);

                }

                else
                {
                    $conrib_data2 = array
                    (
                        'user_id' => $value->user_id,
                        'amount' => 0,
                        'month' => date('m'),
                        'year' => date('Y')
                    );

                    $this->db->insert('contributions', $conrib_data2);
                }

                $this->db->where('user_id', $value->user_id);
                $this->db->set('start_month', 'finished');
                $this->db->update('contributons_track');
            }

        }

        if($next_date->num_rows() > 0)
        {
            foreach ($next_date->result() as $item)
            {
                $wallet_amount_next = $this->General->get_wallet_amount($item->user_id);

                if($wallet_amount_next > $item->amount)
                {
                    $conrib_data_next = array
                    (
                        'user_id' => $item->user_id,
                        'amount' => $item->amount,
                        'month' => date('m'),
                        'year' => date('Y')
                    );

                    $this->db->insert('contributions', $conrib_data_next);

                    $new_wallet_next = $wallet_amount_next - $item->amount;

                    $this->General->update_wallet_direct($item->user_id, $new_wallet_next);

                }

                else
                {
                    $conrib_data_next2 = array
                    (
                        'user_id' => $item->user_id,
                        'amount' => 0,
                        'month' => date('m'),
                        'year' => date('Y')
                    );

                    $this->db->insert('contributions', $conrib_data_next2);
                }

                $next_month = strtotime(date("Y-m-d", strtotime(date('Y-m-d'))) . " +1 month");

                $this->db->where('user_id', $item->user_id);
                $this->db->set('next_month', date('Y-m-d', $next_month));
                $this->db->update('contributons_track');

            }
        }


    }
}