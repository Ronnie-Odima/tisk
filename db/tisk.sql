-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 15, 2018 at 06:27 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tisk`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `id` int(11) NOT NULL,
  `user_id` text NOT NULL,
  `activity` text NOT NULL,
  `amount` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `user_id`, `activity`, `amount`, `date`) VALUES
(1, '15b6aff945e1bd', 'Bought Shares', 500, '2018-08-14 14:53:40'),
(2, '15b6aff945e1bd', 'Bought Shares', 250, '2018-08-14 15:00:17'),
(3, '15b7301fab0f8f', 'Paid Registration Fee', 1000, '2018-08-14 16:41:15'),
(4, '15b7301fab0f8f', 'Bought Shares', 1000, '2018-08-14 16:41:27'),
(5, '15b7301fab0f8f', 'Bought Shares', 500, '2018-08-14 16:44:04'),
(6, '15b7301fab0f8f', 'Bought Shares', 500, '2018-08-14 18:46:32'),
(7, '15b7301fab0f8f', 'Bought Shares', 250, '2018-08-14 19:13:31'),
(8, '15b7301fab0f8f', 'Bought Shares', 250, '2018-08-14 19:48:04');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `c_id` text NOT NULL,
  `name` text NOT NULL,
  `reg_fee` int(11) NOT NULL,
  `share_capital` int(11) NOT NULL,
  `contribution` int(11) NOT NULL,
  `benefit` text NOT NULL,
  `info` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `contributions`
--

CREATE TABLE `contributions` (
  `id` int(11) NOT NULL,
  `user_id` text NOT NULL,
  `amount` int(11) NOT NULL,
  `month` text NOT NULL,
  `year` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contributions`
--

INSERT INTO `contributions` (`id`, `user_id`, `amount`, `month`, `year`, `date`) VALUES
(89, '15b6aff945e1bd', 5000, '08', '2018', '2018-08-14 15:44:15'),
(90, '15b6aff945e1bd', 0, '08', '2018', '2018-08-14 16:12:46'),
(91, '15b7301fab0f8f', 250, '08', '2018', '2018-08-14 16:41:39'),
(92, '15b7301fab0f8f', 250, '08', '2018', '2018-08-14 19:14:11'),
(93, '15b7301fab0f8f', 250, '08', '2018', '2018-08-14 19:15:40'),
(94, '15b6aff945e1bd', 0, '08', '2018', '2018-08-15 14:05:03');

-- --------------------------------------------------------

--
-- Table structure for table `contributons_track`
--

CREATE TABLE `contributons_track` (
  `id` int(11) NOT NULL,
  `user_id` text NOT NULL,
  `amount` int(11) NOT NULL,
  `start_month` text NOT NULL,
  `next_month` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contributons_track`
--

INSERT INTO `contributons_track` (`id`, `user_id`, `amount`, `start_month`, `next_month`) VALUES
(1, '15b6aff945e1bd', 5000, 'finished', '2018-09-15'),
(2, '15b7301fab0f8f', 250, 'finished', '2018-09-14');

-- --------------------------------------------------------

--
-- Table structure for table `corporates`
--

CREATE TABLE `corporates` (
  `id` int(11) NOT NULL,
  `corporate_id` text NOT NULL,
  `name` text NOT NULL,
  `type` text NOT NULL,
  `reg_no` text NOT NULL,
  `reg_date` text NOT NULL,
  `reg_office` text NOT NULL,
  `postal_add` text NOT NULL,
  `physical_add` text NOT NULL,
  `office_tel` text NOT NULL,
  `office_email` text NOT NULL,
  `c_p_name` text NOT NULL,
  `c_p_phone` text NOT NULL,
  `nature` text NOT NULL,
  `acc_no` text NOT NULL,
  `type_id` text,
  `image` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `existing` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `corporates`
--

INSERT INTO `corporates` (`id`, `corporate_id`, `name`, `type`, `reg_no`, `reg_date`, `reg_office`, `postal_add`, `physical_add`, `office_tel`, `office_email`, `c_p_name`, `c_p_phone`, `nature`, `acc_no`, `type_id`, `image`, `date`, `existing`) VALUES
(6, '15b6aff945e1bd', 'E-Kodi', 'company', '223213', '08/01/2018', 'Jubilee HQ', 'dsfdsd', 'dsds', '3424234', 'ekodi@gmail.com', 'Ronnie', '254711259995', 'fdsfds', 'E-Kodi-Tisk', 'terabyte', './assets/images/corp.png', '2018-08-08 14:35:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `counties`
--

CREATE TABLE `counties` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `counties`
--

INSERT INTO `counties` (`id`, `name`) VALUES
(48, 'BOMET'),
(49, 'BUNGOMA'),
(50, 'BUSIA'),
(51, 'ELGEYO/MARAKWET'),
(52, 'EMBU'),
(53, 'GARISSA'),
(54, 'HOMA BAY'),
(55, 'ISIOLO'),
(56, 'KAJIADO'),
(57, 'KAKAMEGA'),
(58, 'KERICHO'),
(59, 'KIAMBU'),
(60, 'KILIFI'),
(61, 'KIRINYAGA'),
(62, 'KISII'),
(63, 'KISUMU'),
(64, 'KITUI'),
(65, 'KWALE'),
(66, 'LAIKIPIA'),
(67, 'LAMU'),
(68, 'MACHAKOS'),
(69, 'MAKUENI'),
(70, 'MANDERA'),
(71, 'MARSABIT'),
(72, 'MERU'),
(73, 'MIGORI'),
(74, 'MOMBASA'),
(75, 'MURANGA'),
(76, 'NAIROBI'),
(77, 'NAKURU'),
(78, 'NANDI'),
(79, 'NAROK'),
(80, 'NYAMIRA'),
(81, 'NYANDARUA'),
(82, 'NYERI'),
(83, 'SAMBURU'),
(84, 'SIAYA'),
(85, 'TAITA TAVETA'),
(86, 'TANA RIVER'),
(87, 'THARAKA - NITHI'),
(88, 'TRANS NZOIA'),
(89, 'TURKANA'),
(90, 'UASIN GISHU'),
(91, 'VIHIGA'),
(92, 'WAJIR'),
(93, 'WEST POKOT'),
(94, 'BARINGO');

-- --------------------------------------------------------

--
-- Table structure for table `innovations`
--

CREATE TABLE `innovations` (
  `id` int(11) NOT NULL,
  `innovation_id` text NOT NULL,
  `solution` text NOT NULL,
  `summary` text NOT NULL,
  `stage` text NOT NULL,
  `category` text NOT NULL,
  `current_value` int(11) NOT NULL,
  `annual_budget` int(11) NOT NULL,
  `request_amount` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `logins`
--

CREATE TABLE `logins` (
  `id` int(11) NOT NULL,
  `user_id` text NOT NULL,
  `email` text NOT NULL,
  `password` text NOT NULL,
  `level` int(11) NOT NULL,
  `approved` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logins`
--

INSERT INTO `logins` (`id`, `user_id`, `email`, `password`, `level`, `approved`) VALUES
(8, '15b6aff945e1bd', 'ekodi@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 3, 0),
(9, '15b7301fab0f8f', 'ronnieodima@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `fname` text NOT NULL,
  `lname` text NOT NULL,
  `email` text NOT NULL,
  `phone` text NOT NULL,
  `n_id` text NOT NULL,
  `gender` text NOT NULL,
  `salutation` text NOT NULL,
  `dob` text NOT NULL,
  `location` text NOT NULL,
  `acc_no` text NOT NULL,
  `type_id` text,
  `user_id` text NOT NULL,
  `image` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `existing` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fname`, `lname`, `email`, `phone`, `n_id`, `gender`, `salutation`, `dob`, `location`, `acc_no`, `type_id`, `user_id`, `image`, `date`, `existing`) VALUES
(1, 'Ronnie', 'Odima', 'ronnieodima@gmail.com', '254711259995', '3432423', 'Male', 'Mr.', '06/06/1994', 'Nairobi', 'Ronnie-Tisk-1', 'megabyte', '15b7301fab0f8f', './assets/images/image.png', '2018-08-14 20:10:03', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_reg_fee`
--

CREATE TABLE `user_reg_fee` (
  `id` int(11) NOT NULL,
  `user_id` text NOT NULL,
  `paid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_reg_fee`
--

INSERT INTO `user_reg_fee` (`id`, `user_id`, `paid`) VALUES
(1, '15b6aff945e1bd', 1),
(2, '15b7301fab0f8f', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_shares`
--

CREATE TABLE `user_shares` (
  `id` int(11) NOT NULL,
  `user_id` text NOT NULL,
  `no_shares` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_shares`
--

INSERT INTO `user_shares` (`id`, `user_id`, `no_shares`, `amount`, `date`) VALUES
(1, '15b6aff945e1bd', 2, 500, '2018-08-13 17:17:30'),
(2, '15b6aff945e1bd', 1, 250, '2018-08-13 17:41:20'),
(4, '15b6aff945e1bd', 2, 500, '2018-08-14 09:07:59'),
(5, '15b6aff945e1bd', 2, 500, '2018-08-14 14:27:30'),
(6, '15b6aff945e1bd', 1, 250, '2018-08-14 14:28:55'),
(7, '15b6aff945e1bd', 2, 500, '2018-08-14 14:30:37'),
(8, '15b6aff945e1bd', 2, 500, '2018-08-14 14:53:40'),
(9, '15b6aff945e1bd', 1, 250, '2018-08-14 15:00:17'),
(10, '15b7301fab0f8f', 4, 1000, '2018-08-14 16:41:27'),
(11, '15b7301fab0f8f', 2, 500, '2018-08-14 16:44:04'),
(12, '15b7301fab0f8f', 2, 500, '2018-08-14 18:46:31'),
(13, '15b7301fab0f8f', 1, 250, '2018-08-14 19:13:31'),
(14, '15b7301fab0f8f', 1, 250, '2018-08-14 19:48:04');

-- --------------------------------------------------------

--
-- Table structure for table `user_wallet`
--

CREATE TABLE `user_wallet` (
  `id` int(11) NOT NULL,
  `user_id` text NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_wallet`
--

INSERT INTO `user_wallet` (`id`, `user_id`, `amount`) VALUES
(2, '15b6aff945e1bd', 5000),
(3, '15b7301fab0f8f', 5750);

-- --------------------------------------------------------

--
-- Table structure for table `wallet_top_ups`
--

CREATE TABLE `wallet_top_ups` (
  `id` int(11) NOT NULL,
  `user_id` text NOT NULL,
  `amount` int(11) NOT NULL,
  `trans_type` text NOT NULL,
  `trans_id` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contributions`
--
ALTER TABLE `contributions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contributons_track`
--
ALTER TABLE `contributons_track`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `corporates`
--
ALTER TABLE `corporates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `counties`
--
ALTER TABLE `counties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `innovations`
--
ALTER TABLE `innovations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logins`
--
ALTER TABLE `logins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_reg_fee`
--
ALTER TABLE `user_reg_fee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_shares`
--
ALTER TABLE `user_shares`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_wallet`
--
ALTER TABLE `user_wallet`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wallet_top_ups`
--
ALTER TABLE `wallet_top_ups`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `contributions`
--
ALTER TABLE `contributions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;
--
-- AUTO_INCREMENT for table `contributons_track`
--
ALTER TABLE `contributons_track`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `corporates`
--
ALTER TABLE `corporates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `counties`
--
ALTER TABLE `counties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;
--
-- AUTO_INCREMENT for table `innovations`
--
ALTER TABLE `innovations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `logins`
--
ALTER TABLE `logins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_reg_fee`
--
ALTER TABLE `user_reg_fee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user_shares`
--
ALTER TABLE `user_shares`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `user_wallet`
--
ALTER TABLE `user_wallet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `wallet_top_ups`
--
ALTER TABLE `wallet_top_ups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
