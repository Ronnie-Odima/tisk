<div class="container">
    <div class="row">
        <div class="col-sm-4" style="margin-bottom: 15px;">
            <div class="card">
                <div class="card-body">
                    <h5>
                        <i class="fa fa-google-wallet"></i>
                        &nbsp;
                        My Wallet Amount
                    </h5>
                    <br>

                    <div id="user_wallet_div"></div>

                    <hr>

                    <br>

                    <h6>
                        <i class="fa fa-check-circle"></i>
                        &nbsp;
                       Top Up Your Wallet
                    </h6>

                    <form action="<?php echo base_url('payments/stkPush')?>" method="post" id="top_up_wallet_form">
                        <div class="form-group">
                            <input type="number" class="form-control" name="amount" placeholder="Enter Amount" required>
                            <input type="hidden" name="phone" value="<?php echo $details[3]['phone'] ;?>">
                            <input type="hidden" name="acc_no" value="<?php echo $details[4]['acc_no']?>">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-danger" type="submit">
                                Top Up Wallet
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="card">
                <div class="card-body">
                    <div>
                        <h6>
                            <b>
                                Registration
                            </b>
                        </h6>
                        <div>
                            Pay nonrefundable entrance fee of <b>KSH. 1000</b>
                        </div>
                        <br>
                        <button class="btn btn btn-danger btn-sm" id="reg_pay_button">
                            Pay Registration
                        </button>
                        <div id="reg_fee_paid_text" style="display: none; ">
                            <h6 class="text-success">
                                Registration Fee Paid
                            </h6>
                        </div>
                    </div>
                    <hr>

                    <div>
                        <h6>
                            <b>
                                Shares
                            </b>
                        </h6>
                        <div>
                            Buy minimum of <b>4 shares</b> at <b>KSH. 250</b> each, to continue.
                        </div>
                        <br>
                        <button class="btn btn btn-danger btn-sm" data-toggle="modal" data-target="#sharesModal" id="buy_shares_btn">
                            Buy Shares
                        </button>
                        <div id="shares_paid_text" style="display: none; ">
                            <h6 class="text-success">
                                Share Bought Already
                            </h6>
                        </div>
                        <form action="<?php echo base_url('payments/buy_shares')?>" method="post" id="buy_shares_form">
                            <div id="sharesModal"
                                 class="modal fade" tabindex="-1" role="dialog"
                                 aria-labelledby="myModalLabel" aria-hidden="true"
                                 style="display: none;">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">
                                                Buy Shares
                                            </h4>
                                            <button type="button" class="close"
                                                    data-dismiss="modal"
                                                    aria-hidden="true">×
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label>
                                                    Number of Shares
                                                </label>
                                                <input type="number" class="form-control" name="shares_no" required>
                                                <input type="hidden" name="user_id" value="<?php echo $this->uri->segment(3) ;?>">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button"
                                                    class="btn btn-default waves-effect btn-sm"
                                                    data-dismiss="modal">Close
                                            </button>

                                            <button type="submit"
                                                    class="btn btn-danger waves-effect waves-light btn-sm">
                                                Purchase
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <hr>

                    <div>
                        <h6>
                            <b>
                                Our Products
                            </b>
                        </h6>
                        <div>
                            This are the types of accounts relevant to your category <b>(<?php echo ucwords($this->uri->segment(4))?>)</b>
                        </div>
                        <br>
                        <div id="accordion">

                            <?php if($this->uri->segment(4) == 'corporate') {?>
                                <div class="card">
                                    <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#terabyte">
                                        <a class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                                           aria-controls="collapseOne">
                                            <h6 class="mb-0">
                                                Terabyte Account
                                            </h6>
                                        </a>
                                    </div>

                                    <div id="terabyte" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="card-body">
                                            <table class="table table-bordered table-hover ">
                                                <tr>
                                                    <td style="width: 40%;">Account Opening Fee</td>
                                                    <td>Ksh. 500</td>
                                                </tr>
                                                <tr>
                                                    <td>Monthly Contribution (Min)</td>
                                                    <td>Ksh. 5000</td>
                                                </tr>
                                                <tr>
                                                    <td>Features</td>
                                                    <td>
                                                        Minimum account balance of Ksh. 60,000 earn interest
                                                        <hr>
                                                        Have access to Sacco as a service product
                                                        <hr>
                                                        Competitive Interest rates from up to 12%
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Policy</td>
                                                    <td>
                                                        At least 2 of its  member has to be a member of the Sacco
                                                        <hr>
                                                        Credit facility available up to 3 times the savings
                                                    </td>
                                                </tr>
                                            </table>
                                            <hr>
                                            <button class="btn btn-outline-danger btn-sm" type="button" id="open_terabyte">
                                                Open Account
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <br>
                            <?php } elseif ($this->uri->segment(4) == 'individual') {?>
                                <div class="card">
                                    <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#petabyte">
                                        <a class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                                           aria-controls="collapseOne">
                                            <h6 class="mb-0">
                                                Petabyte Account
                                            </h6>
                                        </a>
                                    </div>

                                    <div id="petabyte" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="card-body">
                                            <table class="table table-bordered table-hover ">
                                                <tr>
                                                    <td style="width: 40%;">Account Opening Fee</td>
                                                    <td>Ksh. 5,000</td>
                                                </tr>
                                                <tr>
                                                    <td>Monthly Contribution (Min)</td>
                                                    <td>Ksh. 15,000</td>
                                                </tr>
                                                <tr>
                                                    <td>Features</td>
                                                    <td>
                                                        Investment maximum limit is Kshs. 1,000,000
                                                        <hr>
                                                        No monthly contributions  if member has invested the maximum amount
                                                        <hr>
                                                        Funds are not refundable but transferable
                                                        <hr>
                                                        Up to 15% annual returns
                                                        <hr>
                                                        Eligible to all members
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Policy</td>
                                                    <td>
                                                        This account cannot be use to take loan
                                                    </td>
                                                </tr>
                                            </table>
                                            <hr>
                                            <button class="btn btn-outline-danger btn-sm" type="button" id="open_petabyte">
                                                Open Account
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <br>

                                <div class="card">
                                    <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#gigabyte">
                                        <a class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                                           aria-controls="collapseOne">
                                            <h6 class="mb-0">
                                                Gigabyte Account
                                            </h6>
                                        </a>
                                    </div>

                                    <div id="gigabyte" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="card-body">
                                            <table class="table table-bordered table-hover ">
                                                <tr>
                                                    <td style="width: 40%;">Account Opening Fee</td>
                                                    <td>Ksh. 500</td>
                                                </tr>
                                                <tr>
                                                    <td>Monthly Contribution (Min)</td>
                                                    <td>Ksh. 1000</td>
                                                </tr>
                                                <tr>
                                                    <td>Features</td>
                                                    <td>
                                                        Minimum amount of saving that can earn interest is Kshs. 10,000
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Policy</td>
                                                    <td>
                                                        Funds are withdrawable upon one moth notice
                                                    </td>
                                                </tr>
                                            </table>
                                            <hr>
                                            <button class="btn btn-outline-danger btn-sm" type="button" id="open_gigabyte">
                                                Open Account
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <br>

                                <div class="card">
                                    <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#megabyte">
                                        <a class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                                           aria-controls="collapseOne">
                                            <h6 class="mb-0">
                                                Megabyte Account
                                            </h6>
                                        </a>
                                    </div>

                                    <div id="megabyte" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="card-body">
                                            <table class="table table-bordered table-hover ">
                                                <tr>
                                                    <td style="width: 40%;">Account Opening Fee</td>
                                                    <td>Ksh. 0</td>
                                                </tr>
                                                <tr>
                                                    <td>Monthly Contribution (Min)</td>
                                                    <td>Ksh. 250</td>
                                                </tr>
                                                <tr>
                                                    <td>Features</td>
                                                    <td>
                                                        No maximum limit on savings
                                                        <hr>
                                                        Can be used to secure a loan
                                                        <hr>
                                                        Earn attractive annual returns of up to 12%
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Policy</td>
                                                    <td>
                                                        Funds refundable after 60 days of notice to exit
                                                    </td>
                                                </tr>
                                            </table>
                                            <hr>
                                            <button class="btn btn-outline-danger btn-sm" type="button" id="open_megabyte">
                                                Open Account
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <br>

                                <div class="card">
                                    <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#kilobyte">
                                        <a class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                                           aria-controls="collapseOne">
                                            <h6 class="mb-0">
                                                Kilobyte Account
                                            </h6>
                                        </a>
                                    </div>

                                    <div id="kilobyte" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="card-body">
                                            <table class="table table-bordered table-hover ">
                                                <tr>
                                                    <td style="width: 40%;">Account Opening Fee</td>
                                                    <td>Ksh. 500</td>
                                                </tr>
                                                <tr>
                                                    <td>Monthly Contribution (Min)</td>
                                                    <td>Ksh. 500</td>
                                                </tr>
                                                <tr>
                                                    <td>Features</td>
                                                    <td>
                                                        Annual fun day for the Kids
                                                        <hr>
                                                        Free Birthday & Success cards
                                                        <hr>
                                                        7.5% Interest p.a. on savings above Ksh 10,000.00
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Policy</td>
                                                    <td>
                                                        Deposits can be used as Security for principle members loan but, not used to compute the upper loan limit
                                                        <hr>
                                                        One months’ notice is required for withdrawal by principle member
                                                        <hr>

                                                    </td>
                                                </tr>
                                            </table>
                                            <hr>
                                            <button class="btn btn-outline-danger btn-sm" type="button" id="open_kilobyte">
                                                Open Account
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <br>
                            <?php }?>

                        </div>

                    </div>
                    <hr>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url('assets/external/js/jquery.js') ;?>"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#top_up_wallet_form").submit(function (e) {
            e.preventDefault();

            $.ajax({
                type: "POST",
                url: $(this).attr("action"),
                data: $(this).serializeArray(),
                beforeSend: function () {
                    swal({
                        title: "Please Wait",
                        text: "Processing Mpesa payment request...",
                        type: "info",
                        showConfirmButton: false,
                        showCancelButton: false
                    });
                },

                success: function (res) {

                    if(res === '0')
                    {
                        swal({
                            title: "Process Payment",
                            text: "Please accept the Mpesa payment request on your phone to complete your top up",
                            type: "info",
                            showConfirmButton: false,
                            showCancelButton: false
                        });

                        setTimeout(function () {
                            window.location.reload();
                        }, 5000);
                    }

                    else
                    {
                        swal({
                            title: "Processing Error",
                            text: "Payment Request Failed. Try again after some time or refresh this page.",
                            type: "warning",
                            showConfirmButton: true,
                            confirmButtonText: "Ok",
                            confirmButtonColor: ok_btn_color,
                            showCancelButton: false
                        });
                    }
                }
            });
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#reg_pay_button").click(function () {

            $.ajax({
                type: "POST",
                url: "<?php echo base_url('payments/pay_reg_fee/'.$this->uri->segment(3))?>",
                data: $(this).serializeArray(),
                beforeSend: function () {
                    swal({
                        title: "Please Wait",
                        text: "Processing Registration Payment",
                        type: "info",
                        showConfirmButton: false,
                        showCancelButton: false
                    });
                },

                success: function (res) {

                    if(res === 'reg_paid')
                    {
                        swal({
                            title: "Payment Successful",
                            text: "Registration fee paid successfully",
                            type: "info",
                            showConfirmButton: true,
                            confirmButtonText: "Ok",
                            confirmButtonColor: ok_btn_color,
                            showCancelButton: false
                        });

                        setTimeout(function () {
                            window.location.reload();
                        }, 3000);
                    }

                    else
                    {
                        swal({
                            title: "Insufficient Funds",
                            text: "Please top up your wallet first.",
                            type: "warning",
                            showConfirmButton: true,
                            confirmButtonText: "Ok",
                            confirmButtonColor: ok_btn_color,
                            showCancelButton: false
                        });
                    }
                }
            });
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#open_terabyte").click(function () {

            $.ajax({
                type: "POST",
                url: "<?php echo base_url('auth/open_account/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/terabyte')?>",
                data: $(this).serializeArray(),
                beforeSend: function () {
                    swal({
                        title: "Please Wait",
                        text: "Processing Account Info",
                        type: "info",
                        showConfirmButton: false,
                        showCancelButton: false
                    });
                },

                success: function (res) {

                    if(res === 'error')
                    {
                        swal({
                            title: "Account Error",
                            text: "Incorrect Account category",
                            type: "error",
                            showConfirmButton: true,
                            confirmButtonText: "Ok",
                            confirmButtonColor: ok_btn_color,
                            showCancelButton: false
                        });
                    }

                    else if(res === 'no_shares')
                    {
                        swal({
                            title: "No shares",
                            text: "Please buy the minimum required shares first.",
                            type: "warning",
                            showConfirmButton: true,
                            confirmButtonText: "Ok",
                            confirmButtonColor: ok_btn_color,
                            showCancelButton: false
                        });
                    }

                    else
                    {
                        swal({
                            title: "Process Successful",
                            text: "Your Terabyte Account has been created successfully. Redirecting ...",
                            type: "success",
                            showConfirmButton: false,
                            showCancelButton: false
                        });

                        setTimeout(function () {
                            window.location.href = "<?php echo base_url()?>auth/login_redirect/" + res;
                        }, 3000);
                    }
                }
            });
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#open_petabyte").click(function () {

            $.ajax({
                type: "POST",
                url: "<?php echo base_url('auth/open_account/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/petabyte')?>",
                data: $(this).serializeArray(),
                beforeSend: function () {
                    swal({
                        title: "Please Wait",
                        text: "Processing Account Info",
                        type: "info",
                        showConfirmButton: false,
                        showCancelButton: false
                    });
                },

                success: function (res) {


                    if(res === 'error')
                    {
                        swal({
                            title: "Account Error",
                            text: "Incorrect Account category",
                            type: "error",
                            showConfirmButton: true,
                            confirmButtonText: "Ok",
                            confirmButtonColor: ok_btn_color,
                            showCancelButton: false
                        });
                    }

                    else if(res === 'no_shares')
                    {
                        swal({
                            title: "No shares",
                            text: "Please buy the minimum required shares first.",
                            type: "warning",
                            showConfirmButton: true,
                            confirmButtonText: "Ok",
                            confirmButtonColor: ok_btn_color,
                            showCancelButton: false
                        });
                    }

                    else
                    {
                        swal({
                            title: "Process Successful",
                            text: "Your Petabyte Account has been created successfully. Redirecting ...",
                            type: "success",
                            showConfirmButton: false,
                            showCancelButton: false
                        });

                        setTimeout(function () {
                            window.location.href = "<?php echo base_url()?>auth/login_redirect/" + res;
                        }, 3000);
                    }
                }
            });
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#open_gigabyte").click(function () {

            $.ajax({
                type: "POST",
                url: "<?php echo base_url('auth/open_account/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/gigabyte')?>",
                data: $(this).serializeArray(),
                beforeSend: function () {
                    swal({
                        title: "Please Wait",
                        text: "Processing Account Info",
                        type: "info",
                        showConfirmButton: false,
                        showCancelButton: false
                    });
                },

                success: function (res) {

                    if(res === 'error')
                    {
                        swal({
                            title: "Account Error",
                            text: "Incorrect Account category",
                            type: "error",
                            showConfirmButton: true,
                            confirmButtonText: "Ok",
                            confirmButtonColor: ok_btn_color,
                            showCancelButton: false
                        });
                    }

                    else if(res === 'no_shares')
                    {
                        swal({
                            title: "No shares",
                            text: "Please buy the minimum required shares first.",
                            type: "warning",
                            showConfirmButton: true,
                            confirmButtonText: "Ok",
                            confirmButtonColor: ok_btn_color,
                            showCancelButton: false
                        });
                    }

                    else
                    {
                        swal({
                            title: "Process Successful",
                            text: "Your Gigabyte Account has been created successfully.Redirecting ...",
                            type: "success",
                            showConfirmButton: true,
                            confirmButtonText: "Ok",
                            confirmButtonColor: ok_btn_color,
                            showCancelButton: false
                        });

                        setTimeout(function () {
                            window.location.href = "<?php echo base_url()?>auth/login_redirect/" + res;
                        }, 3000);
                    }
                }
            });
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#open_megabyte").click(function () {

            $.ajax({
                type: "POST",
                url: "<?php echo base_url('auth/open_account/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/megabyte')?>",
                data: $(this).serializeArray(),
                beforeSend: function () {
                    swal({
                        title: "Please Wait",
                        text: "Processing Account Info",
                        type: "info",
                        showConfirmButton: false,
                        showCancelButton: false
                    });
                },

                success: function (res) {

                    if(res === 'error')
                    {
                        swal({
                            title: "Account Error",
                            text: "Incorrect Account category",
                            type: "error",
                            showConfirmButton: true,
                            confirmButtonText: "Ok",
                            confirmButtonColor: ok_btn_color,
                            showCancelButton: false
                        });
                    }

                   else if(res === 'no_shares')
                    {
                        swal({
                            title: "No shares",
                            text: "Please buy the minimum required shares first.",
                            type: "warning",
                            showConfirmButton: true,
                            confirmButtonText: "Ok",
                            confirmButtonColor: ok_btn_color,
                            showCancelButton: false
                        });
                    }
                    else
                    {
                        swal({
                            title: "Process Successful",
                            text: "Your Megabyte Account has been created successfully. Redirecting ...",
                            type: "success",
                            showConfirmButton: false,
                            showCancelButton: false
                        });

                        setTimeout(function () {
                            window.location.href = "<?php echo base_url()?>auth/login_redirect/" + res;
                        }, 3000);
                    }
                }
            });
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#open_kilobyte").click(function () {

            $.ajax({
                type: "POST",
                url: "<?php echo base_url('auth/open_account/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'/kilobyte')?>",
                data: $(this).serializeArray(),
                beforeSend: function () {
                    swal({
                        title: "Please Wait",
                        text: "Processing Account Info",
                        type: "info",
                        showConfirmButton: false,
                        showCancelButton: false
                    });
                },

                success: function (res) {

                    if(res === 'error')
                    {
                        swal({
                            title: "Account Error",
                            text: "Incorrect Account category",
                            type: "error",
                            showConfirmButton: true,
                            confirmButtonText: "Ok",
                            confirmButtonColor: ok_btn_color,
                            showCancelButton: false
                        });
                    }

                    else if(res === 'no_shares')
                    {
                        swal({
                            title: "No shares",
                            text: "Please buy the minimum required shares first.",
                            type: "warning",
                            showConfirmButton: true,
                            confirmButtonText: "Ok",
                            confirmButtonColor: ok_btn_color,
                            showCancelButton: false
                        });
                    }

                    else
                    {
                        swal({
                            title: "Process Successful",
                            text: "Your Kilobyte Account has been created successfully. Redirecting ...",
                            type: "success",
                            showConfirmButton: false,
                            showCancelButton: false
                        });

                        setTimeout(function () {
                            window.location.href = "<?php echo base_url()?>auth/login_redirect/" + res;
                        }, 3000);
                    }
                }
            });
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#buy_shares_form").submit(function (e) {
            e.preventDefault();

            $.ajax({
                type: "POST",
                url: $(this).attr("action"),
                data: $(this).serializeArray(),
                beforeSend: function () {
                    swal({
                        title: "Please Wait",
                        text: "Processing shares payment...",
                        type: "info",
                        showConfirmButton: false,
                        showCancelButton: false
                    });
                },

                success: function (res) {

                    if(res === 'no_funds')
                    {
                        swal({
                            title: "Insufficient Funds",
                            text: "Please top up your wallet first.",
                            type: "success",
                            showConfirmButton: true,
                            confirmButtonText: "Ok",
                            confirmButtonColor: ok_btn_color,
                            showCancelButton: false
                        });
                    }

                    else if(res === 'not_paid')
                    {
                        swal({
                            title: "Registration Not Paid",
                            text: "Please pay your registration fee first.",
                            type: "warning",
                            showConfirmButton: true,
                            confirmButtonText: "Ok",
                            confirmButtonColor: ok_btn_color,
                            showCancelButton: false
                        });
                    }

                    else
                    {
                        swal({
                            title: "Successful",
                            text: "Your purchase has been processed successfully",
                            type: "success",
                            showConfirmButton: false,
                            showCancelButton: false
                        });

                        setTimeout(function () {
                            window.location.reload();
                        },2000);
                    }
                }
            });
        });
    });
</script>

<script type="text/javascript">
    (function fetch_user_wallet_amount() {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('auth/user_wallet_amount/'.$this->uri->segment(3))?>",
            success: function (res) {
                $("#user_wallet_div").html(res);
            },
            complete: function () {
                setTimeout(fetch_user_wallet_amount, 1000);
            }
        });
    })();
</script>

<script type="text/javascript">
    (function check_reg_fee_paid() {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('auth/check_reg_fee_paid/'.$this->uri->segment(3))?>",
            success: function (res) {

                if(res === 'paid')
                {
                    $("#reg_pay_button").hide();
                    $("#reg_fee_paid_text").show();
                }
                else
                {
                    $("#reg_pay_button").show();
                    $("#reg_fee_paid_text").hide();
                }
            },
            complete: function () {
                setTimeout(check_reg_fee_paid, 1000);
            }
        });
    })();
</script>

<script type="text/javascript">
    (function check_min_shares_bought() {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('auth/check_min_shares_bought/'.$this->uri->segment(3))?>",
            success: function (res) {

                if(res === 'min_passed')
                {
                    $("#buy_shares_btn").hide();
                    $("#shares_paid_text").show();
                }
                else
                {
                    $("#buy_shares_btn").show();
                    $("#shares_paid_text").hide();
                }
            },
            complete: function () {
                setTimeout(check_min_shares_bought, 1000);
            }
        });
    })();
</script>